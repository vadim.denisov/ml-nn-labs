import glob
import logging
import os
import tarfile
from pathlib import Path
from typing import Optional

import numpy as np
import requests

import imageio

DATA_DIR = Path.cwd() / 'data'


def download(url: str, output: Path):
    output.parent.mkdir(parents=True, exist_ok=True)
    if not output.exists():
        logging.info('Download from %s', url)
        targz = requests.get(url)
        with open(output, 'wb') as f:
            f.write(targz.content)
    logging.info('Download complete: %s', output)


def unarchive(path: Path, outdir: Path, *, name: str):
    output = outdir / name
    if not output.exists():
        logging.info('Extract from %s', path)
        with tarfile.open(path, 'r') as f:
            f.extractall(outdir)
    logging.info('Extract copmlete: %s', outdir)


class Dataset:
    def __init__(self, X: np.array, Y: np.array):
        self._X = X
        self._Y = Y

    @classmethod
    def copy(cls, dataset: 'Dataset') -> 'Dataset':
        return cls(dataset.X, dataset.Y)

    @classmethod
    def load(cls, name: str, *,
             url: str,
             ext: str = 'tar.gz',
             data_dir: Path = DATA_DIR) -> 'Dataset':
        archive_path = data_dir / f'{name}.{ext}'
        download(url, archive_path)
        unarchive(archive_path, data_dir, name=name)

        return cls.parse_not_mnist(dataset_dir=data_dir / name)

    @classmethod
    def parse_not_mnist(cls, dataset_dir: Path) -> 'Dataset':
        X, Y = [], []
        for label in sorted(os.listdir(dataset_dir)):
            label_data_glob = str(dataset_dir / label / '*.png')

            for img_path in glob.glob(label_data_glob):
                logging.debug('handle %s', img_path)
                x_row = cls._img_to_numpy(img_path)
                if x_row is None:
                    continue
                X.append(x_row)
                Y.append(label)

        X = np.array(X)
        Y = np.array(Y).reshape(len(X), 1)
        return cls(X=X, Y=Y)

    @property
    def X(self) -> np.array:
        return self._X

    @property
    def Y(self) -> np.array:
        return self._Y

    @staticmethod
    def _img_to_numpy(img_path) -> Optional[np.array]:
        try:
            return imageio.imread(img_path)
        except ValueError as error:
            logging.warning(f'{img_path}: {error}')
            return None
